

export function getSearch(): string {
    let params = new URLSearchParams(window.location.search)
    return params.get('q')
}
