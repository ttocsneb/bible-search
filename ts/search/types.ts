export interface SearchItem {
    type: 'item'
    value: string
}

export interface SearchGroup {
    type: 'group'
    value: SearchOr
}

export type SearchValue = SearchItem | SearchGroup

export interface Search {
    ands: Array<SearchValue>
    negs: Array<SearchValue>
}

export type SearchOr = Array<Search>

