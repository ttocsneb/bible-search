import Vue from 'vue'
import axios from 'axios'
import Search from './search.vue'
import Results from './results.vue'


interface Verse {
    book: string,
    chapter: number,
    verse: number,
    text: string
}


interface SearchResult {
    query: string,
    page: number,
    page_size: number,
    results: number,
    corrected: {[key: string]: string}
    unknown: Array<string>
    elapsed: number,
    result: Array<Verse>
}


new Vue({
    el: '#search',
    components: {
        'search': Search,
        'results': Results
    },
    data() {
        return {
            time: 0,
            results: 0,
            corrected: '',
            corrected_orig: '',
            result: [],
            search: ''
        }
    },
    methods: {
        do_search(search: string) {
            axios.get('/bible/search', {
                params: {
                    q: search
                }
            }).then(response => {
                let results = response.data as SearchResult
                this.time = results.elapsed
                this.results = results.results
                this.result = results.result
                this.corrected_orig = Object.keys(results.corrected)[0]
                this.corrected = results.corrected[this.corrected_orig]
                this.search = search
            })
        }
    }
})