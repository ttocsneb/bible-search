import $ from 'jquery'
import _ from 'lodash'


interface Select extends HTMLElement {
    value: string
}


/**
 * Called when a button is clicked
 * 
 * @param action action
 */
function button(action: string) {
    let search = new URLSearchParams();
    search.set('action', action);
    (window as any).location = `${window.location.pathname}?${search}`;
}


const actions = ['book-dec', 'book-inc', 'chapter-dec', 'chapter-inc']

for(let action of actions) {
    $(`#${action}`).on('click', () => {
        button(action)
    })
}

$('#id_book').on('change', (event) => {
    let target = event.target as Select
    window.location.pathname = `/read/${target.value}/`
})
$('#id_chapter').on('change', (event) => {
    let target = event.target as Select
    let location = _.filter(window.location.pathname.split('/'), val => Boolean(val))
    location.pop()
    location.push(target.value)
    window.location.pathname = _.join(location, '/')
})
