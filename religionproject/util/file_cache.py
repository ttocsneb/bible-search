import logging
import time
from django.conf import settings

import os


class Cached:
    """
    Caches objects in the filesystem.

    If the object is not cached, then when the item is loaded, it will first
    cache the item, save it, then load the item. After this first cache, the
    item will not be loaded again unless invalidated.
    """

    @staticmethod
    def default_writer(obj, f):
        f.write(str(obj))

    @staticmethod
    def default_reader(f):
        return f.read()

    def __init__(self, name: str, loader: callable, writer: callable = None, reader: callable = None):
        self.name = name
        self.loader = loader
        self.writer = writer or self.default_writer
        self.reader = reader or self.default_reader
        self.cached = False
    
    def invalidate(self):
        self.cached = False

    def load(self):
        file_name = os.path.join(settings.CACHE_DIR, self.name)
        if not self.cached or settings.DEBUG:
            # cache the object.
            start = time.time()
            obj = self.loader(self.name)
            duration = time.time() - start
            logging.getLogger(__name__).info("Loaded %s after %f", self.name, duration)
            # Check that the parent directory exists
            if not os.path.exists(os.path.dirname(file_name)):
                os.makedirs(os.path.dirname(file_name))
            # Write the cached object to file
            with open(file_name, 'w') as f:
                self.writer(obj, f)
            self.cached = True

        # load the object.
        with open(file_name) as f:
            return self.reader(f)


class CachedDecorator(Cached):
    """
    A decorator for a cached object

    :param func: decorated function
    :param name: name of the cached object
    :param loader: function to load the object
    :param writer: function to write the object to the cache
    :param reader: function to read the object from the cache

    .. note::

        loader, writer, and reader are also decorators:

        .. code-block:: python

            @CachedDecorator('foobar')
            def foobar(cache, args):
                # TODO Do stuff with cache
            
            @foobar.loader
            def foobar_loader(name: str):
                # TODO Load `name` object

            @foobar.writer
            def foobar_writer(obj, f):
                f.write(obj)
            
            @foobar.reader
            def foobar_reader(f):
                return f.read()

    """

    class _Property_Decorator:
        def __init__(self, default: callable = None):
            def required(*args, **kwargs):
                raise NotImplementedError("This function is required")

            self.func = default or required

        def __call__(self, func: callable):
            self.func = func

    def __init__(self, func: callable, name: str, loader: callable = None, writer: callable = None, reader: callable = None):
        super().__init__(
            name,
            self._Property_Decorator(loader),
            self._Property_Decorator(writer or Cached.default_writer),
            self._Property_Decorator(reader or Cached.default_reader)
        )
        self.func = func

    def load(self):
        file_name = os.path.join(settings.CACHE_DIR, self.name)
        if not self.cached:
            # cache the object.
            obj = self.loader.func(self.name)
            # Check that the parent directory exists
            if not os.path.exists(os.path.dirname(file_name)):
                os.makedirs(os.path.dirname(file_name))
            # Write the cached object to file
            with open(file_name, 'w') as f:
                self.writer.func(obj, f)

        # load the object.
        with open(file_name) as f:
            return self.reader.func(f)

    def __call__(self, *args, **kwargs):
        self.func(self.load(), *args, **kwargs)
