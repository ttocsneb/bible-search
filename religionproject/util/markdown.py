import os
import markdown2

from django.conf import settings

from . import file_cache


markdowns = {}


def _markdown_loader(path: str):
    file_path = os.path.join(settings.MARKDOWN_DIR, path)
    return markdown2.markdown_path(file_path)


def markdown(path: str):
    # Check if the cacher is in the markdowns dict
    if path not in markdowns:
        markdowns[path] = file_cache.Cached(path, _markdown_loader)

    return markdowns[path]


def load_markdown(path: str):
    return markdown(path).load()