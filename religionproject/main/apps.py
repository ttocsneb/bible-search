from django.apps import AppConfig


class MainConfig(AppConfig):
    name = 'religionproject.main'
    label = 'main'
