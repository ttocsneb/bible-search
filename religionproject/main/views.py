from django.shortcuts import render, redirect
from django.urls import reverse
from urllib.parse import urljoin
from django.http.request import HttpRequest
from django.http.response import Http404

from . import forms
from religionproject.bible import models


def main(request: HttpRequest):
    return render(request, 'document.html.j2', {
        'active': 'main',
        'md': 'main.md'
    })


def read(request: HttpRequest, book: str, chapter: int):
    form = forms.ChapterSelect({
        'book': book,
        'chapter': chapter
    })

    if not form.is_valid():
        raise Http404()

    chapter: models.ChapterInfo = form.cleaned_data['chapter']
    book: models.BookInfo = form.cleaned_data['book']

    def index_of(items: list, value):
        try:
            return next(i for i, v in enumerate(items) if value == v[0].instance)
        except StopIteration as e:
            raise IndexError from e
    
    def at(items: list, index: int):
        try:
            return next(v for i, v in enumerate(items) if i == index)
        except StopIteration as e:
            raise IndexError from e

    chapter_index = index_of(form.fields['chapter'].choices, chapter)
    chapter_len = len(form.fields['chapter'].choices)
    book_index = index_of(form.fields['book'].choices, book)
    book_len = len(form.fields['book'].choices)

    end_books = book_index + 1 >= book_len
    beg_books = book_index <= 0
    end_chapters = chapter_index + 1 >= chapter_len
    beg_chapters = chapter_index <= 0

    action = request.GET.get('action', None)
    if action in ['chapter-inc', 'chapter-dec', 'book-inc', 'book-dec']:
        if 'inc' in action:
            value = 1
        else:
            value = -1
        if 'chapter' in action:
            if end_chapters and value > 0:
                book = at(form.fields['book'].choices, book_index + value)[0].instance
                chapter = '1'
            elif beg_chapters and value < 0:
                book = at(form.fields['book'].choices, book_index + value)[0].instance
                chapter = book.chapter_set.last()
            else:
                chapter = at(form.fields['chapter'].choices, chapter_index + value)[0].instance
        else:
            book = at(form.fields['book'].choices, book_index + value)[0].instance
            chapter = '1'
        return redirect('read_chapter', book.slug, chapter)


    return render(request, 'read.html.j2', {
        'title': 'Read',
        'active': 'read',
        'chapter_inc_disabled': end_books and end_chapters,
        'chapter_dec_disabled': beg_books and beg_chapters,
        'book_inc_disabled': end_books,
        'book_dec_disabled': beg_books,
        'verses': list(form.cleaned_data['chapter'].verse_set.all()),
        'form': form
    })


def read_redirect(request: HttpRequest, book: str = None):
    if book is None:
        found = models.BookInfo.objects.order_by('order').first()
    else:
        found = models.BookInfo.objects.filter(name__slug=book).first()
    if not found:
        raise Http404()
    chapter: models.ChapterInfo = found.chapter_set.first()
    return redirect('read_chapter', found.slug, chapter)



def search(request: HttpRequest):
    return render(request, 'search.html.j2', {
        'title': 'Search',
        'active': 'search'
    })
