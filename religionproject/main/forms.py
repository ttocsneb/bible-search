from django import forms

from religionproject.bible import models


class ChapterSelect(forms.Form):
    book = forms.ModelChoiceField(models.BookInfo.objects.all(), initial=lambda: models.BookInfo.objects.first(), required=True, to_field_name='name__slug')
    chapter = forms.ModelChoiceField(models.ChapterInfo.objects.all(), initial='1', required=True, to_field_name='chapter')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['chapter'].queryset = models.ChapterInfo.objects.filter(
            book__name__slug=self.data['book']
        ).all()

        self.fields['chapter'].initial = self.fields['chapter'].queryset.first()
