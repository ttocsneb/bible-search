from django.urls.conf import path
from . import views


urlpatterns = [
    path('', views.main, name='main'),
    path('read/', views.read_redirect, name='read'),
    path('read/<slug:book>/', views.read_redirect, name='read_book'),
    path('read/<slug:book>/<int:chapter>/', views.read, name='read_chapter'),
    path('search/', views.search, name='search')
]
