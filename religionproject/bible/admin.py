from django.contrib import admin

from . import models


admin.site.register(models.BookInfo)
admin.site.register(models.ChapterInfo)
admin.site.register(models.Verse)
admin.site.register(models.VerseData)
admin.site.register(models.Token)
