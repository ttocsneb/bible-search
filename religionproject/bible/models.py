from django.db import models


class SlugLookup(models.Lookup):
    lookup_name = 'slug'

    def as_sql(self, compiler, connection):
        lhs, lhs_params = self.process_lhs(compiler, connection)
        rhs, rhs_params = self.process_rhs(compiler, connection)
        params = [p.replace('-', ' ') for p in lhs_params + rhs_params]
        return 'UPPER(%s) = UPPER(%s)' % (lhs, rhs), params

    @property
    def output_field(self):
        return models.CharField()


models.CharField.register_lookup(SlugLookup)


class BookInfo(models.Model):
    """
    Information for a book

    Stores the name of the book
    """
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=32, unique=True)
    order = models.IntegerField()

    @property
    def chapters(self) -> models.QuerySet:
        return self.chapter_set.get_queryset()

    @property
    def slug(self) -> str:
        return self.name.lower().replace(' ', '-')

    @property
    def name__slug(self) -> str:
        return self.slug

    @classmethod
    def load_books(cls, books: list) -> (
            models.Model, models.Model, models.Model):
        books_objs = []
        chapters = []
        verses = []
        for book in books:
            b, cs, vs = cls.load_book(book)
            books_objs.append(b)
            chapters.extend(cs)
            verses.extend(vs)
        return books_objs, chapters, verses

    def __str__(self):
        return self.name

    def __repr__(self):
        return '<BookInfo: {}>'.format(
            self.name,
        )


class ChapterInfo(models.Model):
    """
    Information for a chapter

    Stores the chapter number and the book it belongs to
    """
    id = models.AutoField(primary_key=True)
    book = models.ForeignKey(BookInfo, related_name='chapter_set',
                             on_delete=models.CASCADE)
    chapter = models.IntegerField()

    @property
    def verses(self) -> models.QuerySet:
        return self.verse_set.get_queryset()

    def __str__(self):
        return str(self.chapter)

    def __repr__(self):
        return '<ChapterInfo: {} {}>'.format(
            self.book.name,
            self.chapter,
        )


class VerseData(models.Model):
    """
    Stores a portion of a verse.

    This model is required to allow for virtually infinite lenghts of data.
    """
    id = models.AutoField(primary_key=True)
    next = models.ForeignKey('bible.VerseData', null=True, default=None,
                             on_delete=models.CASCADE)
    data = models.CharField(max_length=255)

    def save(self, *args, **kwargs):
        if self.next is not None:
            self.next.save()
        super().save(*args, **kwargs)

    @property
    def text(self) -> str:
        if self.next is not None:
            return self.data + self.next.text
        return self.data

    @text.setter
    def text(self, text: str):
        self.data = text[0:255]
        extra_data = text[255:]
        if extra_data:
            if self.next is None:
                self.next = VerseData()
            self.next.text = extra_data


class Verse(models.Model):
    """
    A single verse in the bible

    Stores the verse number, text, and chapter it belongs to
    """
    id = models.AutoField(primary_key=True)
    chapter = models.ForeignKey(ChapterInfo, related_name='verse_set',
                                on_delete=models.CASCADE)
    verse = models.IntegerField()
    data = models.ForeignKey(VerseData, on_delete=models.CASCADE, null=True)

    class Meta:
        permissions = [('can_load', 'Can Load Bible')]

    @property
    def text(self) -> str:
        return self.data.text

    def save(self, *args, **kwargs):
        if self.data is not None:
            self.data.save()
        super().save(*args, **kwargs)

    @text.setter
    def text(self, value: str):
        if self.data is None:
            self.data = VerseData()
        self.data.text = value

    def __repr__(self):
        return '<Verse: {} {}:{}>'.format(
            self.chapter.book.name,
            self.chapter.chapter,
            self.verse
        )


class Token(models.Model):
    """
    An index object to help aid in searches
    """
    token = models.CharField(max_length=32, primary_key=True)
    verses = models.ManyToManyField(Verse)

    def __repr__(self):
        return '<Token: {}>'.format(
            self.token
        )
