import time
from django.http.request import HttpRequest
from django.http.response import JsonResponse, Http404
from . import loader, models
from .search import parser, correct_tokenize
from django.contrib.auth.decorators import login_required, permission_required
from django.db.models import ObjectDoesNotExist


@login_required
@permission_required('bible.can_load')
def read_bible(request: HttpRequest):
    """
    Load the bible into the database
    """
    start = time.time()
    books, chapters, verses, tokens = loader.load_bible(
        request.GET.get('file', "data/raw/bible.tsv")
    )
    elapsed = time.time() - start

    return JsonResponse({
        'books': books,
        'chapters': chapters,
        'verses': verses,
        'tokens': tokens,
        'elapsed': elapsed
    })


def search(request: HttpRequest):
    """
    search for some verses
    """

    start_time = time.time()

    query_text = request.GET.get('q', '')
    book_filters = request.GET.getlist('bf', '')
    chapter_filters = request.GET.getlist('cf', '')

    query = parser.parse_query(query_text)

    try:
        page = int(request.GET.get('p', '0'))
    except ValueError:
        page = 0
    try:
        page_size = int(request.GET.get('n', '10'))
    except ValueError:
        page_size = 10

    if isinstance(query, str):
        return JsonResponse({
            'error': query
        })
    else:
        start = page * page_size
        raw_data = sorted(
            list(query.process((book_filters, chapter_filters))),
            key=lambda v: v.chapter_id
        )
        data = raw_data[start:start + page_size]
        total = len(raw_data)
    elapsed = time.time() - start_time

    _, corrected, unknown = correct_tokenize(query_text)

    return JsonResponse({
        'query': str(query),
        'page': page,
        'page_size': page_size,
        'results': total,
        'corrected': corrected,
        'unknown': unknown,
        'elapsed': elapsed,
        'result': [
            {
                'book': q.chapter.book.name,
                'chapter': q.chapter.chapter,
                'verse': q.verse,
                'text': q.text,
            } for q in data]
    }, json_dumps_params={'indent': 2})


def read(request: HttpRequest, book: str = None, chapter: int = None):
    if not book:
        # return book list
        books = models.BookInfo.objects.all()

        return JsonResponse({
            'books': [
                {
                    'slug': b.slug,
                    'name': b.name,
                    'i': b.order
                }
                for b in books
            ]
        })
    try:
        book_obj = models.BookInfo.objects.get(
            name__iexact=book.replace('-', ' ')
        )
    except ObjectDoesNotExist:
        raise Http404('book does not exist')
    if not chapter:
        # return chapter list
        return JsonResponse({
            'book': book_obj.name,
            'chapters': [c.chapter for c in book_obj.chapters]
        })
    # return verses
    try:
        chapter_obj: models.ChapterInfo = book_obj.chapter_set.get(
            chapter=chapter
        )
    except ObjectDoesNotExist:
        raise Http404('chapter does not exist')

    verses = chapter_obj.verses

    return JsonResponse({
        'book': book_obj.name,
        'chapter': chapter_obj.chapter,
        'verses': [
            {
                'verse': v.verse,
                'text': v.text
            }
            for v in verses
        ]
    })
