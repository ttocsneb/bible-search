from .. import models

from django.db import transaction


def build_verse(chapter: models.ChapterInfo, verse: dict):
    v = models.Verse(
        chapter=chapter,
        verse=verse['verse'],
    )
    v.text = verse['text']
    return v


def build_chapter(book: models.BookInfo, chapter: dict):
    chapter_obj = models.ChapterInfo(book=book, chapter=chapter['chapter'])
    verses = [
        build_verse(chapter_obj, v)
        for v in chapter['verses']
    ]
    return chapter_obj, verses


def build_book(book: dict):
    book_obj = models.BookInfo(name=book['book'], order=book['order'])
    chapters = []
    verses = []
    for chapter in book['chapters']:
        c, vs = build_chapter(book_obj, chapter)
        chapters.append(c)
        verses.extend(vs)
    return book_obj, chapters, verses


def build_books(books: list):
    books_objs = []
    chapters = []
    verses = []
    for book in books:
        b, cs, vs = build_book(book)
        books_objs.append(b)
        chapters.extend(cs)
        verses.extend(vs)
    return books_objs, chapters, verses


@transaction.atomic
def build_and_replace_books(books: list):
    models.BookInfo.objects.all().delete()
    models.ChapterInfo.objects.all().delete()
    models.Verse.objects.all().delete()
    models.VerseData.objects.all().delete()

    bs, cs, vs = build_books(books)

    for b in bs:
        b.save()
    for c in cs:
        c.save()
    for v in vs:
        v.save()
    
    return bs, cs, vs
