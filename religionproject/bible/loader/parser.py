from marshmallow import Schema, fields, EXCLUDE
import csv
import re

from collections import OrderedDict


class Groups(fields.List):
    """
    Group A list of data into groups of lists of data
    """
    def __init__(self, key: str, out: str, *args, **kwargs):
        """
        :param key: key to group by
        :param out: key to output with
        """
        super().__init__(*args, **kwargs)
        self._key = key
        self._out = out

    def _deserialize(self, values, attr, data, **kwargs):
        grouped = OrderedDict()
        for value in values:
            key = value[self._key]
            if key not in grouped:
                grouped[key] = []
            grouped[key].append(dict(
                (k, v) for k, v in value.items() if k != key
            ))
        groups = [
            {
                self._key: k,
                self._out: v
            }
            for k, v in grouped.items()
        ]
        return super()._deserialize(groups, attr, data, **kwargs)


class Verse(Schema):
    verse = fields.Integer()
    text = fields.String()


class Chapter(Schema):
    chapter = fields.Integer()
    verses = fields.Nested(Verse, many=True, unknown=EXCLUDE)


class Book(Schema):
    book = fields.String()
    chapters = Groups('chapter', 'verses', fields.Nested(Chapter))


class Books(Schema):
    books = Groups('book', 'chapters', fields.Nested(Book))


def read(path: str):
    """
    Read the bible into a format compatible with the database models
    """
    with open(path) as f:
        reader = csv.reader(f, delimiter='\t')
        verses = list(reader)

    location_regex = re.compile(r"(.+?)\s(\d+):(\d+)")

    new_verses = []

    for location, verse in verses:
        b, c, v = next(re.finditer(location_regex, location)).groups()
        new_verses.append({
            'book': b,
            'chapter': c,
            'verse': v,
            'text': verse
        })

    schema = Books()

    books = schema.load({
        'books': new_verses
    })['books']

    for i, book in enumerate(books):
        book['order'] = i

    return books
