from django.db import transaction
import logging

from .. import models, search


def index_verse(verse: models.Verse) -> int:
    """
    Index tokens in a verse

    :param verse: verse to index

    :return: then number of tokens created
    """

    created_tokens = 0

    for token in search.tokenize(verse.text):
        t, n = models.Token.objects.get_or_create(token=token)
        if n:
            created_tokens += 1
        t.verses.add(verse)
        t.save()

    return created_tokens


@transaction.atomic
def index_all(verses: models.Verse) -> int:
    logger = logging.getLogger(__name__)

    models.Token.objects.all().delete()

    created_tokens = 0
    total = len(verses)
    for i, verse in enumerate(verses):
        tokens = index_verse(verse)
        created_tokens += tokens
        if i % 100 == 0:
            logger.info(
                '%d%% (%d/%d), created %d words',
                (i / total) * 100,
                i,
                total,
                created_tokens
            )
    logger.info('added %d tokens', created_tokens)

    return created_tokens
