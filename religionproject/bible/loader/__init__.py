from . import builder, indexer, parser
from django.db import transaction
import logging


@transaction.atomic
def load_bible(path: str):
    """
    Load the bible into the database

    :param path: path to load

    :return: number of items created
    """
    logger = logging.getLogger(__name__)
    parsed = parser.read(path)
    books, chapters, verses = builder.build_and_replace_books(parsed)
    logger.info("loaded %d books, %d chapters, and %d verses", len(books), len(chapters), len(verses))

    tokens = indexer.index_all(verses)

    return len(books), len(chapters), len(verses), tokens
