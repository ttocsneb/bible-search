from typing import Set, List, Tuple

from .. import models

from . import tokenize


def get_tokens_any(text: str) -> List[models.Token]:
    """
    Get all the tokens in the search string

    :param text: search string

    :return: list of Tokens
    """
    return list(
        models.Token.objects.filter(
            token__in=tokenize(text)
        )
    )


def search_intersect(text: str, filters: Tuple[List[str], List[int]] = ([], [])) -> Set[models.Verse]:
    """
    Search for all verses that contain all the tokens in the search string

    :param text: search string
    :param filters: filters

    :return: list of verses
    """
    intersect: Set[models.Verse] = set()
    for token in get_tokens_any(text):
        if any(filters):
            filter_dict = {}
            if filters[0]:
                filter_dict['chapter__book__name__iregex'] = '({})'.format('|'.join(filters[0]))
            if filters[1]:
                filter_dict['chapter__chapter__in'] = filters[1]
            verses = token.verses.filter(
                **filter_dict
            ).order_by('chapter__book__order')
        else:
            verses = token.verses.all().order_by('chapter__book__order')
        
        if not intersect:
            # Get all verses in the first token
            intersect = set(verses)
        else:
            # Add all verses that are also in the intersect
            intersect = set(verses.filter(
                id__in=[i.id for i in intersect]
            ))
        # If there are no more verses in the intersect, don't check any more
        # tokens
        if not intersect:
            return intersect
    
    return intersect


def search_phrase(text: str, filters: Tuple[List[str], List[int]] = ([], [])) -> Set[models.Verse]:
    """
    Search for all verses that contain the phrase

    :param text: search phrase

    :return: list of verses
    """
    verses: Set[models.Verse] = set()
    phrase = ' '.join(tokenize(text))
    for verse in search_intersect(text):
        tokenized = ' '.join(tokenize(verse.text))
        if phrase in tokenized:
            verses.add(verse)
    return verses
