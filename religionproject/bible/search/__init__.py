import re

from typing import List, Dict

from . import spell


tokenizer_re = (
    r"(\w+)"  # Any word
    r"(?:[^\w\s](\w+))?"  # Skip single letter punctuations
)


def tokenize(text: str) -> List[str]:
    """
    Take a string, and convert it into tokens

    A token is any word without punctuation

    :param text: input string

    :return: list of tokens
    """
    return [
        ''.join(i).lower()
        for i in re.findall(tokenizer_re, text)
    ]


def correct_tokenize(text: str) -> (List[str], Dict[str, str], List[str]):
    tokens = tokenize(text)
    new_tokens = list()
    corrected = dict()
    unknown = list()
    for t in tokens:
        correct = spell.correctWord(t)
        if not correct:
            unknown.append(t)
            new_tokens.append(t)
            continue
        if t != correct:
            corrected[t] = correct
        new_tokens.append(correct)
    return new_tokens, corrected, unknown
