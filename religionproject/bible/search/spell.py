
from typing import Set, Union
from .. import models
from django.db.models import ObjectDoesNotExist


def editDistance(word: str) -> Set[str]:
    edit = set()
    for i in range(len(word)):
        # deletion
        edit.add(word[0:i] + word[i+1:])

        # alteration
        for c in range(0, 26):
            c = chr(ord('a') + c)
            edit.add(word[0:i] + c + word[i+1:])

    for i in range(len(word) - 1):
        # transposition
        edit.add(word[0:i] + word[i+1] + word[i] + word[i+2:])

    for i in range(len(word) + 1):
        # insertion
        for c in range(0, 26):
            c = chr(ord('a') + c)
            edit.add(word[0:i] + c + word[i:])

    return edit


def checkWord(word: str) -> int:
    try:
        return models.Token.objects.get(token=word).verses.count()
    except ObjectDoesNotExist:
        return 0


def mostCorrect(words: Set[str]) -> Union[str, None]:
    capped = list(words)[:50000]
    print("Capped: %d" % len(capped))
    corrects = models.Token.objects.filter(token__in=capped)
    correct = set()

    count = 1
    for word in corrects:
        c = word.verses.count()
        if c > count:
            correct.clear()
            count = c
            correct.add(word.token)
        elif c == count:
            correct.add(word.token)
    return next(iter(correct), None)


def correctWord(word: str) -> Union[str, None]:
    word = word.lower()
    if checkWord(word) > 0:
        return word
    if len(word) > 32:
        return None
    editDistance1 = editDistance(word)
    print('EditDist1: %d' % len(editDistance1))

    correct = mostCorrect(editDistance1)
    if correct:
        return correct
    if len(word) > 10:
        return None
    editDistance2 = set()
    for w in editDistance1:
        editDistance2.update(editDistance(w))
    editDistance2.difference_update(editDistance1)
    print('EditDist2: %d' % len(editDistance2))

    return mostCorrect(editDistance2)
