from ... import models

from .. import query

from typing import Set, List, Tuple


def indent(text: str, tabs: int = 1, tab: str = '  '):
    return '\n'.join(
        '{}{}'.format(tab * tabs, t)
        for t in str(text).split('\n')
    )


class Search:
    def process(self, filters: Tuple[List[str], List[int]]) -> Set[models.Verse]:
        """
        Process the search

        :return: list of verses
        """
        raise NotImplementedError


class Normal(Search):
    def __init__(self, *args: str):
        self.text = ' '.join(args)

    def process(self, filters: Tuple[List[str], List[int]]) -> Set[models.Verse]:
        return set(query.search_intersect(self.text, filters))

    def __str__(self):
        return self.text


class Phrase(Search):
    def __init__(self, *args: str):
        self.text = ' '.join(args)

    def process(self, filters: Tuple[List[str], List[int]]) -> Set[models.Verse]:
        return set(query.search_phrase(self.text, filters))

    def __str__(self):
        return '"{}"'.format(self.text)


class And(Search):
    def __init__(self, *children: Search):
        self.children = children

    def process(self, filters: Tuple[List[str], List[int]]) -> Set[models.Verse]:
        intersect = set()
        for child in self.children:
            if not intersect:
                intersect = child.process(filters)
            else:
                intersect.intersection_update(child.process)
            if not intersect:
                break
        return intersect

    def __str__(self):
        return ' & '.join(
            str(a) for a in self.children
        )


class Negate(Search):
    def __init__(self, ands: And, *negs: Search):
        self.negs = negs
        self.ands = ands

    def process(self, filters: Tuple[List[str], List[int]]) -> Set[models.Verse]:
        # If no and is provided, negate every verse
        if not self.ands.children:
            items = set(models.Verse.objects.all())
        else:
            items = self.ands.process(filters)
        # For each negate search, take the difference of the base
        for neg in self.negs:
            items.difference_update(neg.process(filters))
        return items

    def __str__(self):
        negs = ['-({})'.format(i) for i in self.negs]
        ands = [str(i) for i in self.ands.children]
        return ' & '.join(ands + negs)


class Or(Search):
    def __init__(self, *children: Search):
        self.children = children

    def process(self, filters: Tuple[List[str], List[int]]) -> Set[models.Verse]:
        union = set()
        for child in self.children:
            union.update(child.process(filters))
        return union

    def __str__(self):
        if len(self.children) > 1:
            return '({})'.format(" | ".join(
                str(c) for c in self.children
            ))
        return " | ".join(
            str(c) for c in self.children
        )
