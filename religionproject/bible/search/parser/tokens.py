import re

from collections import OrderedDict

AND = 'AND'
OR = 'OR'
PHRASE = 'PHRASE'
NEGATE = 'NEGATE'
SEARCH = 'SEARCH'
IGNORE = 'IGNORE'
PAREN_OPEN = 'PAREN_OPEN'
PAREN_CLOSE = 'PAREN_CLOSE'


def token_str(token: str):
    def search(text: str) -> (str, int):
        if text.startswith(token):
            return token, len(token)

    return search


def token_regex(regex: str, process: callable = None):
    def default_process(match):
        return match[0]
    process = process or default_process

    def search(text: str) -> (str, int):
        match = re.match(regex, text)
        if match is None:
            return None
        return process(match), match.end()

    return search


tokens = OrderedDict(
    AND=token_str('&'),
    OR=token_str('|'),
    PHRASE=token_regex(r'([\'"])(.+?)\1', lambda g: g[2]),
    NEGATE=token_str('-'),
    PAREN_OPEN=token_str('('),
    PAREN_CLOSE=token_str(')'),
    SEARCH=token_regex(r'[^&|()"\s]+'),
    IGNORE=token_regex(r'.')
)


class Token:
    def __init__(self, token: str, text: str, pos: int):
        self.token = token
        self.text = text
        self.pos = pos

    @classmethod
    def parse_next(cls, text: str, pos: int = 0):
        """
        Parse the next token into a string
        """
        for k, v in tokens.items():
            token = v(text)
            if token is not None:
                return cls(k, token[0], pos), token[1]
        raise ValueError

    @classmethod
    def parse(cls, text: str):
        """
        Parse a string into a list of tokens

        :param text: text

        :return: list of tokens
        """
        tokens = []
        pos = 0
        while text:
            t, s = cls.parse_next(text, pos)
            if t.token != IGNORE:
                tokens.append(t)
            text = text[s:]
            pos += s

        return tokens

    def __repr__(self):
        return '<Token: {} - {} @ {}>'.format(
            self.token,
            self.text,
            self.pos
        )
