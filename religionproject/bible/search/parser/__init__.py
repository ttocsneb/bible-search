from . import tokens, reader, exceptions


def parse_query(text: str):
    """
    Parse a text query

    :param text: query

    :return: set of verses, or string error message
    """
    toks = tokens.Token.parse(text)
    try:
        query = reader.Or.parse(toks)
    except exceptions.TokenException as te:
        return te.format(text)

    return query[0]
