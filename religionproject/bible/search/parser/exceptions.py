from . import tokens


class TokenException(Exception):
    def __init__(self, token: tokens.Token, message: str):
        self.token = token
        self.message = message

    def format(self, text: str, pre: str = '') -> str:
        """
        Format the token exception

        :param text: The original text
        :param pre: text to prepend the message with

        :return: formatted error message
        """
        return (
            '{pre}{}\n'
            '{pre}{}^\n'
            '{pre}{}'
        ).format(
            text,
            ' ' * self.token.pos,
            self.message,
            pre=pre
        )

    def __str__(self):
        return '{}: {}'.fomrat(self.token, self.message)
