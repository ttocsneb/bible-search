from .tokens import Token, OR, PHRASE, NEGATE, SEARCH, PAREN_OPEN, PAREN_CLOSE
from . import objects, exceptions

from typing import List, Union


def checkToken(tokens: List[Token], expected: Union[List[str], str],
               index: int = 0):
    if isinstance(expected, str):
        expected = [expected]
    try:
        return tokens[index].token in expected
    except IndexError:
        return False


class Search:
    @staticmethod
    def check(tokens: List[Token]) -> bool:
        return checkToken(tokens, SEARCH)

    @staticmethod
    def parse(tokens: List[Token]) -> (objects.Normal, int):
        items = []
        for token in tokens:
            if token.token == SEARCH:
                items.append(token)
            else:
                break
        return objects.Normal(*[t.text for t in items]), len(items)


class Group:
    @staticmethod
    def check(tokens: List[Token]) -> bool:
        return checkToken(tokens, PAREN_OPEN)

    @staticmethod
    def parse(tokens: List[Token]) -> (objects.Or, int):
        i, n = Or.parse(tokens[1:])
        n += 1
        if checkToken(tokens, PAREN_CLOSE, n):
            n += 1
        return i, n


class SearchItem:
    @staticmethod
    def check(tokens: List[Token]) -> bool:
        return checkToken(tokens, (SEARCH, PHRASE, PAREN_OPEN))

    @staticmethod
    def parse(tokens: List[Token]) -> (objects.Search, int):
        if Search.check(tokens):
            return Search.parse(tokens)
        elif Group.check(tokens):
            return Group.parse(tokens)
        elif checkToken(tokens, PHRASE):
            return objects.Phrase(tokens[0].text), 1
        raise exceptions.TokenException(tokens[0], "Unexpected Token")


class And:
    @staticmethod
    def check(tokens: List[Token]) -> bool:
        return SearchItem.check(tokens) or checkToken(tokens, NEGATE)

    @classmethod
    def parse_next(cls, tokens: List[Token]) -> (
            List[Search], List[Search], int):
        ands = []
        negs = []
        n = 0
        if checkToken(tokens, NEGATE):
            i, n = SearchItem.parse(tokens[1:])
            n += 1
            negs.append(i)
        elif SearchItem.check(tokens):
            i, n = SearchItem.parse(tokens)
            ands.append(i)
        else:
            return ands, negs, n
        next_ands, next_negs, next_n = cls.parse_next(tokens[n:])
        ands.extend(next_ands)
        negs.extend(next_negs)
        n += next_n
        return ands, negs, n

    @classmethod
    def parse(cls, tokens: List[Token]) -> (
            Union[objects.Negate, objects.And], int):
        ands, negs, n = cls.parse_next(tokens)
        ands = objects.And(*ands)
        if negs:
            if not ands:
                return objects.Negate(None, *negs), n
            return objects.Negate(ands, *negs), n
        else:
            return ands, n


class Or:
    @staticmethod
    def check(tokens: List[Token]) -> bool:
        return And.check(tokens)

    @staticmethod
    def parse(tokens: List[Token]) -> (objects.Or, int):
        ands = []
        n = 0
        while And.check(tokens[n:]):
            i, next_n = And.parse(tokens[n:])
            ands.append(i)
            n += next_n
            if checkToken(tokens, OR, n):
                n += 1
            else:
                break

        return objects.Or(*ands), n
