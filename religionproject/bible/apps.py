from django.apps import AppConfig


class BibleConfig(AppConfig):
    name = 'religionproject.bible'
    label = 'bible'
