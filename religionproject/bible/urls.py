
from django.urls import path
from . import views

urlpatterns = [
    path('load', views.read_bible, name="load"),
    path('search', views.search, name="search"),
    path('books/<slug:book>/<int:chapter>', views.read, name="verses"),
    path('books/<slug:book>', views.read, name="chapter"),
    path('books', views.read, name="books")
]
