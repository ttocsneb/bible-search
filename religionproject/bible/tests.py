from django.test import TestCase

from .search import parser


class TestParser(TestCase):
    def test_simple_query(self):
        self.assertEqual(str(parser.parse_query('hello')), 'hello')
        self.assertEqual(str(parser.parse_query('hello bar')), 'hello bar')
        self.assertEqual(str(parser.parse_query('"hello"')), '"hello"')
        self.assertEqual(str(parser.parse_query('"hello" bar')), '"hello" & bar')
    
    def test_neg_query(self):
        self.assertEqual(str(parser.parse_query('-bar')), '-(bar)')
        self.assertEqual(str(parser.parse_query('foo -bar')), 'foo & -(bar)')
    
    def test_groups_query(self):
        self.assertEqual(str(parser.parse_query('(bar | five) asdf')), '(bar | five) & asdf')
        self.assertEqual(str(parser.parse_query('(bar five) asdf')), 'bar five & asdf')
    
    def test_advanced_query(self):
        self.assertEqual(str(parser.parse_query('foobar -(asdf | zxcv)')), 'foobar & -((asdf | zxcv))')
        self.assertEqual(str(parser.parse_query('foobar -asdf -zxcv')), 'foobar & -(asdf) & -(zxcv)')