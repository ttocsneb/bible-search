from django.templatetags.static import static
from django.urls import reverse
from django.conf import settings

from jinja2 import Environment

from django.forms import BoundField

from .util import markdown

from collections import Iterable

import json


class AttributeList(list):
    def __init__(self, iterable=None):
        if isinstance(iterable, str) or \
                not isinstance(iterable, Iterable):
            super().__init__([iterable])
        else:
            super().__init__(iterable)

    def __str__(self):
        return ' '.join(self)


def set_attr(bound_field: BoundField, attr: str, value: str):
    """
    Set an attribute of a field

    :param bound_field: field to set the attribute
    :param attr: Name of the attribute
    :param value: Value to set to

    :return: bound_field
    """
    bound_field.field.widget.attrs[attr] = AttributeList(value)
    return bound_field


def add_class(bound_field: BoundField, value: str):
    """
    Add a class to the field

    :param bound_field: field to set the attribute
    :param value: class to add

    :return: bound_field
    """

    try:
        bound_field.field.widget.attrs['class'].append(value)
    except KeyError:
        bound_field.field.widget.attrs['class'] = AttributeList(value)
    return bound_field


def script(*args: str) -> set:
    """
    Load a script from webpack

    :param args: Names of the scripts to load

    :return: set of script paths
    """

    with open(settings.WEBPACK_MANIFEST) as f:
        manifest: dict = json.load(f)

    def find_script(script: str) -> list:
        found_chunks = []
        for chunk, bundle in manifest.items():
            if script in chunk:
                found_chunks.extend(bundle['js'])
        return found_chunks

    found_chunks = set()
    for script in args:
        found_chunks.update(find_script(script))
    return found_chunks


def environment(**options):
    env = Environment(**options)
    env.globals.update({
        'static': static,
        'url': reverse,
        'markdown': markdown.load_markdown,
        'script': script
    })
    env.filters.update({
        'set_attr': set_attr,
        'add_class': add_class,
        'json': lambda obj: json.dumps(obj, default=lambda o: str(o))
    })
    return env
