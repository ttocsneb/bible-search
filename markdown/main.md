# What is Bible Search

Bible Search is a web-app that I have developed for a religion class. Our goal
is to create a tangeable product using our own skills that relates to the new
testament. This app allows you to search through the bible as if you were
searching on google.

You can search for any text you like, and Bible Search will find all the verses
that match your query.

----

# FAQ

#### How Do I Search?

To start a search, from any page on the site, enter a query in the search bar
and press search. From there, you will be taken to a page that shows all the
verses that match your search as well as the ability to alter your query.

#### Do I Have To Search The Bible?

Of course not! If you just wanted to read the bible, you can head over to the 
[read page][read] where you can read the entire book.

#### How Did You Get The Bible

I was able to download the King James Bible from [truth.info][truth].

#### Where Can I Get Progress Updates On Your App?

I am keeping track of my progress on [trello][trello].

#### Where Can I Download Your Code?

My code is not yet public, but I do plan to make it public once I finish the
project.

[read]: /read
[trello]: https://trello.com/b/fFY1AtKw/religion-project
[truth]: http://truth.info/download/bible.htm