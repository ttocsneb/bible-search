const path = require("path")
const VueLoaderPlugin = require("vue-loader/lib/plugin")
const Chunks2JsonPlugin = require("chunks-2-json-webpack-plugin")
const { CleanWebpackPlugin } = require("clean-webpack-plugin")

const exclude = [
    /node_modules/
]

const publicPath = '/static/'
const outputDir = 'dist'

module.exports = {
    mode: "development",
    entry: {
        read: "read/index.ts",
        search: "search/index.ts"
    },
    output: {
        path: path.resolve(__dirname, outputDir),
        filename: "[name].[hash].bundle.js",
        publicPath,
        library: "WorkSite",
        libraryTarget: "umd"
    },
    optimization: {
        splitChunks: {
            chunks: 'all'
        }
    },
    module: {
        rules: [
            {
                test: /\.(t|j)s$/,
                loader: "ts-loader",
                exclude: exclude,
                options: {
                    appendTsSuffixTo: [/\.vue$/]
                }
            },
            {
                test: /\.vue$/,
                loader: "vue-loader",
                exclude: exclude
            }
        ]
    },
    resolve: {
        modules: [
            path.resolve(__dirname, "node_modules"),
            path.resolve(__dirname, "ts")
        ],
        extensions: [".js", ".ts", ".vue"],
        alias: {
            'vue$': 'vue/dist/vue.esm.js'
        }
    },
    devtool: "inline-source-map",
    target: "web",
    externals: {
        jquery: "jQuery"
    },
    plugins: [
        new VueLoaderPlugin(),
        new Chunks2JsonPlugin({outputDir: '', publicPath}),
        new CleanWebpackPlugin()
    ]
}
